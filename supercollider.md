# SuperCollider

* Live with whatever the sclang program gives you in terms of formatting (literal tabs, etc.)
* Prefer to use `param: value` notation when specifying Synth arguments, except where obvious
* Use a new line for each `arg` entry, and have a blank line before moving on to the body of a SynthDef.
* Declare `var`s when first defined
* Use parentheses: `1+2*3` evaluates to `9`, not `7`; You probably want `1+(2*3)`
* In SynthDefs, prefer reassigning an intermediate variables, instead of deeply nested functional style, when composing operations.

## Synth argument naming conventions

* alive: set to zero to kill
* buf: buffer number
* gain: master gain
* gate: master gate (may also kill)
* in: input bus
* out: output bus
