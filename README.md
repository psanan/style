# Patrick Sanan's Style Guides

Default style choices and guidelines.

The goal is to reduce cognitive load when reading and writing.

Consistency within a specific project supercedes these choices.

## Meta Style Guide

* Be concise.
    - Include motivation only for non-intuitive or "gotcha" situations.
    - Only address "real" problems.
* Do not include direct examples of incorrect usage.
* Specify a default style guide whenever possible
    - Prefer widely-used, standard, or uncontroversial default style guides.
    - Do not reiterate default style guides.
    - Avoid overriding default style guides.
* Prefer style choices consistent with common automatic formatting tools.
* Specify a default formatting tool when possible.
