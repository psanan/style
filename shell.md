# Shell scripts

Default style guide (Bash): [Google](https://google.github.io/styleguide/shellguide.html)
Style checker: [ShellCheck](https://www.shellcheck.net/).

Note: This guide concerns scripts and libraries and uses bash. For command line instructions
which may be copied into an arbitrary shell, prefer [POSIX `sh` features](http://shellhaters.org/), and ideally those which are supported by all common shells (`bash`, `zsh`, `fish`, etc.).
For scripts intended for maximum portability, one may also specify them to be run
as POSIX `sh` scripts.

Prefer POSIX-compliant `printf` usage to `echo`.

Use `for i in $(seq 1 10)` to iterate over a range, e.g.
```bash
for i in $(seq 0 120); do printf "\033["$i"m$i""\033[0m "; done; printf "\n"
```
