# General Code

## Enforcement

When possible, enforce, or at least check, style automatically.

## Formatting

Prefer a width of 80 characters.

Never leave trailing whitespace.

Never use literal tabs, unless strictly required (e.g. in a Makefile).

Avoid non-ASCII characters.

Prefer functions that fit onto one "screen" (Less than about 80 lines).

Prefer files than one might scroll through and grok in a couple of
minutes, say 500 lines of C, or less for more terse languages.

## Literals

For floating-point literals, include at least one digit after
the decimal point (e.g. `1.0`, not `1`).

## Naming

When no other guidelines apply, prefer

* `snake_case` for filenames and identifiers, including functions in languages (e.g. Python) which treat them as variables
* `CapitalizedCamelCase` for class names
* `CapitalizedCamelCase` for functions, in languages which treat them specially (e.g. C++, Java)

Prefer to use full words in names.

When working with low-level numerical routines, in is acceptable to use
single-character or single-greek-letter "words", in analogy with mathematical usage.
These may be capitalized, since this is often relevant in mathematical formulations.
Common mathematical abbreviations (e.g. `dim` or `max`) are also acceptable.
```
double max, x, x_max, X, X_max, rho, drho, Rho, dRho;
```

For iterator variables, prefer `d` for loops over dimension, and `i`,`j`,...
for loops over arbitrary-sized arrays.

When no other guidelines apply, avoid names which start with underscores, or which
use more than one underscore in a row. These are hard to read and in sometimes reserved.

## Comments

Avoid separator comments, especially those which might clash with default Git
merge annotations (repeated `<`,`>`,or `=`). Prefer to judiciously use blank lines.

Use `TODO` comments as markers for items to be resolved quickly (i.e. before
making a Git commit).

Use `FIXME` comments for longer-term personal use (i.e. to be resolved before
merging code into an integration branch).

## Scripts

Let the user's environment pick the executable to use to run, e.g.
```bash
#!/usr/bin/env sh
```
