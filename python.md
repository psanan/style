# Python Code

Default style guide: [Google](http://google.github.io/styleguide/pyguide.html)

Prefer to use available tools to automatically check or enforce style, e.g.

* [pylint](https://www.pylint.org/)
* [flake8](https://flake8.pycqa.org/en/latest/)
* [yapf](https://pypi.org/project/yapf/)
* [pytype](https://github.com/google/pytype)
* [mypy](http://mypy-lang.org/)
* [vermin](https://pypi.org/project/vermin/)
* [autopep8](https://github.com/hhatto/autopep8)

## Floor division

Always use `\\` when floor/"integer" division is expected.

## Quotes

Prefer to use double quotes and escape literal double quotes. This reduces
friction with languages like C or Julia where single quotes are for characters,
and allows unescaped apostrophes.

## Dependencies

Do not globally install non-standard packages. Instead, specify a `requirements.txt`
and use a [virtual environment](https://docs.python.org/3/tutorial/venv.html).
See [this script](https://gitlab.com/psanan/req2env).
