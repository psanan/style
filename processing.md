# Processing Code

Default style guide: [Google](https://google.github.io/styleguide/javaguide.html)

* Live with whatever the Processing.app command-T auto-format gives you (tabs, split `} else {`, etc.)

* Use `lowerCamelCase` for method names and `snake_case` for variable names.
* To save space, it's acceptable to give default values to primitive types as they are declared in a class (e.g. `int num_things = 3`), but do NOT do this
  with any other type (e.g. anything with a constructor).

* Vectors use units scaled to `width`. Draw with respect to these (use `height/width` to specify relative to `height`), or in absolute pixel sizes (noting that this might create overlap problems).
* When drawing, always set `rectMode()` and `ellipseMode()` explicitly.
* Reset to `noStroke()` if using `stroke()`.
* Reset to `strokeWeight(1)` if using `strokeWeight()`.
* Reset to `colorMode(HSB)` if using `colorMode()`.
* Reset to a default if calling `textFont()`
* Reset to a default if calling `textSize()`
