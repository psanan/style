# Makefiles

## Naming

Top-level makefiles should be called `Makefile`.

## Formatting

Include a space after a target and the colon.
```makefile
foo : bar
```

If using logical blocks, indent with two spaces
```makefile
ifeq($(FOO),$(BAR))
  BAZ = baz
endif

```
