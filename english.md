# English Writing

Default style guide: [Strunk and White, "The Elements of Style"](https://en.wikipedia.org/wiki/The_Elements_of_Style)

## Rules of Thumb

* Shorter is better.
* The goal is to efficiently transmit information to the reader.
* Avoid hedging with parentheticals.

## Formatting

Use `Title Case` for titles.
Prefer `Sentence case` for headers.

## Lists

* If a list has the form of a normal sentence, punctuate it as such.
```
I would like to
1. go to the store,
2. eat lunch, and
3. go to sleep.
```
* If list entries are complete sentences, punctuate them as such. The introductory sentence should be complete and end with a colon.
```
She lived by two maxims:
1. The early bird gets the worm.
2. Worms aren't all they're cracked up to be.
```
* If list entries are not complete sentences, do not include periods.

## Initialisms and acronyms

Spell out all but the most standard acronyms and initialisms the first time they are used, e.g.

    The Ultra Trail du Mont Blanc (UTMB)

Treat an initialism or acronym as an atomic entity for the purpose of pluralization, e.g.

    Physics is often described with PDEs. After discretization, problems are
    often reduced to working with a finite number of DOFs.

Prefer capitalized versions of initialisms, e.g. `DOF` or `3D`, but follow
common usage for acronyms, e.g. `radar`.

## Dates

Default to using ISO 8601 date formatting, e.g. `2020-01-01`

## Technical documents

Pros and cons are not a good construct, since it's not clear what the reference is. Rather rate things by relevant criteria.
