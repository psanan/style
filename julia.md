Default style guide: [Blue](https://github.com/invenia/BlueStyle)

Automatic formatting with 
  * [JuliaFormatter.jl](https://github.com/domluna/JuliaFormatter.jl)
  * [JuliaFormatter.vim](https://github.com/kdheepak/JuliaFormatter.vim)
