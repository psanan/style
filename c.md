# C Code

Default style guide: [PETSc](https://docs.petsc.org/en/latest/developers/style.html)

## Function signatures

In definitions, place pointer asterisks with the argument name, e.g. `int foo(double *x, int y)`.

## Variable declarations

Only declare variables at the top of a block (just after an opening brace)

Align variable names
```C
int    a,b;
double y;
```

## Variable scope

Prefer to declare variables in the narrowest possible scope, using extra braces if needbe to define local scopes
(though consider if these might be better as standalone functions).

## Pointer declarations

Place asterisks with variable names for pointers, and use a separate line for pointers.
Align the asterisk as if it were a normal character.
```C
double x;
int    a,b;
int    *c,*d;
```

When dealing with constant pointers, put a space on both sides of the asterisk,
and if using a pointer to a constant, put `const` before the type
```C
int * const       x;  // a const pointer to a (non-const) int
const int         *y; // a (non-const) pointer to a const int
const int * const z;  // a const pointer to a const int
```

## Initialization

Avoid initializing non-constant variables when they are declared.
```C
int       x;
const int y = 3;

x = 3;
```

## Headers

Use quotes for headers defined locally, and angle brackets for everything else.

Order headers "local to global", starting with a header directly corresponding to a source file,
and ending with standard system includes. This is intended to expose missing includes in the more "local"
headers.

```C
#include "thisclass.h"
#include "myutils.h"
#include <petsc.h>
#include <stdio.h>
```

## Linking

Unless you are actually administering a system with multiple users, prefer `-rpath` to
`LD_LIBRARY_PATH`. (e.g. [as described here](https://www.cprogramming.com/tutorial/shared-libraries-linux-gcc.html))

## C Preprocessor

CPP variables:

* should be namespaced e.g. `PROJECT_NAME_XYZ`.
* should not include double underscores or leading underscores
